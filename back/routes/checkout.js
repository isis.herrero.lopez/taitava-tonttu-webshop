const { findMail, checkProfile } = require("../scripts/scripts");
const { checkCart, saveOrders, emptyCart } =require("../scripts/moreScripts");
const express = require("express");
const route = express.Router();

route.get("/", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const cart = await checkCart(username);
    const finalPrice = req.session.total;
    const profile = await checkProfile(username);
    res.render("../../public/views/partials/checkout.ejs", {profile: profile, cart:cart, finalPrice: finalPrice});
});

route.post("/payment", async (req, res) =>{
    const date = new Date();
    const user = await findMail(req.session.userId);
    const username = user.username;
    const {productName, productColors, productQuantity, productPrice, finalPrice} = req.body;
    const {name, shipAddress, billAddress, email} = req.body;
    const {cardType, cardNumber, month, year, cvv, cardName} = req.body;
    let products = {productName: productName, productColors: productColors, productQuantity: productQuantity, productPrice: productPrice}
    saveOrders(date, username, name, shipAddress, billAddress, email, products, finalPrice, cardType, cardNumber, month, year, cvv, cardName);
//here, it should be included a mailto, confirming the order, both the the client and to the shop owner
    await emptyCart(username);
    res.redirect("/home");
});

route.post("/cancel", (req, res) => {
    res.redirect("/cart");
});

module.exports = route;