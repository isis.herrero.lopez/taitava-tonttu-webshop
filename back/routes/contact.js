const { feedback, printFb } = require("../scripts/scripts");
const express = require("express");
const route = express.Router();

route.get("/", async (req, res) => {
    const fb = await printFb();
    res.render("../../public/partials/contact.ejs", {fb: fb});
});

route.post("/sendFb", (req, res) => {
    const {name, message} = req.body;
    feedback(name, message);
    res.redirect("/contact");
});

module.exports = route;