const bcrypt = require("bcrypt");
const mongodb = require("mongodb").MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = "localhost:27017";
const DB = "dB";
const userPwd = "joulu:tonttu";

let dbo;

mongodb.connect(`mongodb://${userPwd}@${url}/${DB}?authSource=admin`, {useNewUrlParser: true, useUnifiedTopology: true}, (err, db) => {
    dbo = db.db("dB");
});

const saltRounds = 12;

//WISHLIST functions
const saveWishlist = (username, photo, productName, price, colors, quantity) => {
        const product = {photo: photo, productName: productName, price: price, colors: colors, quantity: quantity}
        dbo.collection("wishlist").insertOne({username: username, products: [product]}, function(err, result) {
            if (err) throw err;
            console.log("new wishlist! 1 product added");
        });
}

const checkWishlist = (username) => {
    return new Promise (resolve => {
            dbo.collection("wishlist").findOne({username: username}, function(err, result) {
                if (err) throw err;
                resolve(result)
            });
        });  
}

const updateWishlist = (username, photo, productName, price, colors, quantity) => {
        const product = {photo: photo, productName: productName, price: price, colors: colors, quantity: quantity}
        dbo.collection("wishlist").updateOne({username: username}, {$push: {products: product}}, function(err, result) {
            if (err) throw err;
            console.log("old wishlist - 1 product added");
        });
}

const addComments = (username, productName, colors, comment) =>{
        dbo.collection("wishlist").updateOne({username: username, "products.productName": productName, "products.colors": colors}, {$set: {"products.$.comment": comment}}, function(err, result) {
            if (err) throw err;
            console.log("old wishlist - comments added");
        });
}

const deleteFromWishlist = (username, productName, colors) => {
    return new Promise (resolve => {
        dbo.collection("wishlist").updateOne({username: username}, {$pull: {products: {productName: productName, colors: colors}}}, function(err, result) {
            if (err) throw err;
            resolve(result);
            console.log("wishlist - 1 product deleted");
        });
    });
}

const printWishlist = (username) => {
    return new Promise (resolve => {
            dbo.collection("wishlist").find({username: username}).toArray((err, result) => {
                if (err) throw err;
                resolve(result);
            });
        });
}

const emptyWishlist = (username) => {
    return new Promise (resolve => {
        dbo.collection("wishlist").deleteOne({username: username}, function(err, result) {
            if (err) throw err;
            console.log("wishlist empty");
            resolve(result);
        })
    })
}

const checkWishlistProducts = (username, productName, colors) => {
    return new Promise (resolve => {
            dbo.collection("wishlist").findOne({username: username, "products.productName": productName, "products.colors": colors}, function(err, result) {
                if (err) throw err;
                resolve(result);
            });
        });  
}

const quantityWishlist = (username, productName, colors, newQuantity) => {
    dbo.collection("wishlist").updateOne({username: username, "products.productName": productName, "products.colors": colors}, {$set: {"products.$.quantity": newQuantity}}, function(err, result) {
        if (err) throw err;
        console.log("duplicated product - quantity updated");
    });
}

//CART functions
const addToCart = (username, photo, productName, price, colors, quantity) => {
    const product = {photo: photo, productName: productName, price: price, colors: colors, quantity: quantity};
    dbo.collection("cart").insertOne({username: username, products: [product]}, function(err, result) {
        if (err) throw err;
    });  
}

const checkCart = (username) => {
    return new Promise (resolve => {
            dbo.collection("cart").findOne({username: username}, function(err, result) {
                if (err) throw err;
                resolve(result);
        });     
    });
}

const addMoreToCart = (username, photo, productName, price, colors, quantity) => {
    const product = {photo: photo, productName: productName, price: price, colors: colors, quantity: quantity};
    dbo.collection("cart").updateOne({username: username}, {$push: {products: product}}, function(err, result) {
        if (err) throw err;
    });  
}

const deleteFromCart = (username, productName, colors) => {
    return new Promise (resolve => {
        dbo.collection("cart").updateOne({username: username}, {$pull: {products: {productName: productName, colors: colors}}}, function(err, result) {
            if (err) throw err;
            resolve(result);
        }); 
    });
}

const emptyCart = (username) => {
    return new Promise (resolve => {
        dbo.collection("cart").deleteOne({username: username}, function(err, result) {
            if (err) throw err;
            resolve(result);
        })
    })
}

const checkCartProducts = (username, productName, colors) => {
    return new Promise (resolve => {
        dbo.collection("cart").findOne({username: username, "products.productName": productName, "products.colors": colors}, function(err, result) {
            if (err) throw err;
            resolve(result);
        });
    });  
}

const quantityCart = (username, productName, colors, newQuantity) => {
    dbo.collection("cart").updateOne({username: username, "products.productName": productName, "products.colors": colors}, {$set: {"products.$.quantity": newQuantity}}, function(err, result) {
        if (err) throw err;
    });
}

//ORDERS functions:
const saveOrders = (date, username, name, shipAddress, billAddress, email, products, finalPrice, cardType, cardNumber, month, year, cvv, cardName) => {
    let number;
    bcrypt.hash(cardNumber, saltRounds, function(err, hash) {
         number = hash;

         const userInfo = {name: name, shipAddress: shipAddress, billAddress: billAddress, email: email}
         const cardInfo = {cardType: cardType, cardNumber: number, cardDate: month + '-' + year, cvv: cvv, cardName: cardName }
         const total = {finalPrice: finalPrice}
         dbo.collection("orders").insertOne({date: date, username: username, orders: [userInfo, products, total, cardInfo]}, function(err, result) {
             if (err) throw err;
             console.log("new order submitted!");
         });
    });
}

const printOrders = (username) => {
    return new Promise (resolve => {
            dbo.collection("orders").find({username: username}).toArray((err, result) => {
                if(err) throw err;
                resolve(result);
        });
    });
}


module.exports = {saveWishlist, checkWishlist, updateWishlist, addComments, deleteFromWishlist, printWishlist, emptyWishlist, checkWishlistProducts, quantityWishlist, addToCart, checkCart, addMoreToCart, deleteFromCart, emptyCart, checkCartProducts, quantityCart, saveOrders, printOrders};