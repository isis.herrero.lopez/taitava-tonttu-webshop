const express = require("express");
const route = express.Router();

route.get("/", (req, res) => {
    res.render("../../public/partials/index.ejs");
});

module.exports = route;
