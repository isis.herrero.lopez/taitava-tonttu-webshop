function setMainHeight() {
  const height = window.innerHeight;

  const main = document.getElementsByTagName("main")[0];
  if (window.matchMedia("(max-width: 375px)").matches){
    mainHeight = height - (100 + 10);
  } else if (window.matchMedia("(min-width: 769px").matches) {
    mainHeight = height - (130 + 10);
  } else {
    mainHeight = height - (120 + 10);
  }
  main.style.height = mainHeight + "px";
}

window.onload = setMainHeight;
window.onresize = setMainHeight;