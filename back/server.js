const express = require("express");
const app = express();
const session = require ("express-session");
const MongoStore = require("connect-mongo")(session);
const cookieParser = require("cookie-parser");
 
const indexRoute = require("./routes/index");
const loginRoute = require("./routes/login");
const homeRoute = require("./routes/home");
const contactRoute = require("./routes/contact");
const productsRoute = require("./routes/products");
const cartRoute =require("./routes/cart");
const checkoutRoute =require("./routes/checkout");

const {guestCartExists, copyCartToUser, deleteGuestCart} = require("./scripts/guestScripts");
const {checkCart, checkCartProducts, addMoreToCart, quantityCart} = require("./scripts/moreScripts");

const PORT = 7000;

app.use("/", express.static(__dirname + "/../public"));
app.use(cookieParser("jouluTonttu"));
app.use(express.urlencoded({ extended: true }));
app.use(session({
    name: "thatCookie",
    resave: true,
    saveUninitialized: false,
    rolling: true,
    secret: "tonttu",
    cookie: {
        //maxAge: 28 * 24 * 3600000, //4 weeks
        sameSite: true,
        httpOnly: true,
        secure: false, //needs to be false for non-https
    },
    store: new MongoStore({url: "mongodb://joulu:tonttu@localhost:27017/dB?authSource=admin"})
}));

const reqSession = async (req, res, next) => {
    if (req.session.userId) {
        const username = req.session.username;
        const guest = req.signedCookies.thisCookie;
        const guestCart = await guestCartExists(guest);
        const userCart = await checkCart(username);
        if (guestCart !== null && userCart === null) {
            //add to cart 
            const guestProducts = guestCart.products;
            copyCartToUser(username, guestProducts);
            deleteGuestCart(guest);
        } else if (guestCart !== null && userCart !== null) {
            const guestProducts = guestCart.products;
            for (let i = 0; i < guestProducts.length; i++) {
                const productName = guestProducts[i].productName;
                const colors = guestProducts[i].colors;
                const result = await checkCartProducts(username, productName, colors);
                if (result === null) {
                    const photo = guestProducts[i].photo;
                    const price = guestProducts[i].price;
                    const quantity = guestProducts[i].quantity;
                    addMoreToCart(username, photo, productName, price, colors, quantity);
                } else {
                    let newQuantity = +guestProducts[i].quantity + +result.products[0].quantity;
                    quantityCart(username, productName, colors, newQuantity); 
                }
            }
            deleteGuestCart(guest);
        }
        next();
    } else {
        res.redirect("/");
    }
}

app.use(async(req, res, next) => {
    app.locals.user = req.session.username;
    if (req.session.username === undefined) {
        if (!req.signedCookies.thisCookie) {
            const cookieName = new Date().getTime().toString();
            res.cookie("thisCookie", cookieName, {signed: true, maxAge: 28 * 24 * 3600000, httpOnly: true});
        }
    } else {
        //res.clearCookie('thisCookie');
    }
    next();
});

//Routes
app.use("/", indexRoute);
app.use("/login", loginRoute);
app.use("/home", reqSession, homeRoute);
app.use("/contact", contactRoute);
app.use("/products", productsRoute);
app.use("/cart", cartRoute);
app.use("/checkout", reqSession, checkoutRoute);

app.get("/logout", reqSession, (req, res) => {
    req.session.destroy(err => {
        if (err) {
            res.redirect("/home");
        } else {
            res.clearCookie("thatCookie", { path: "/" });
            res.redirect("/");
        }
    });
})
 
app.listen(PORT, () => {
    console.log("Listening to port " + PORT);
});
