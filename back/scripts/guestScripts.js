const mongodb = require("mongodb").MongoClient;
const url = "localhost:27017";
const DB = "dB";
const userPwd = "joulu:tonttu";

let dbo;

mongodb.connect(`mongodb://${userPwd}@${url}/${DB}?authSource=admin`, {useNewUrlParser: true, useUnifiedTopology: true}, (err, db) => {
    dbo = db.db("dB");
});

const guestCartExists = (username) => {
    return new Promise(resolve => {
        dbo.collection("guest").findOne({username}, function (err, result) {
            if (err) throw err;
            resolve(result);
        });
    });
}

const createGuestCart = (username, photo, productName, price, colors, quantity) => {
    const product = {photo, productName, price, colors, quantity};
    dbo.collection("guest").insertOne({username, products: [product]}, function(err, result) {
        if (err) throw err;
    });
}

const checkGuestCart = (username, productName, colors) => {
    return new Promise(resolve => {
        dbo.collection("guest").findOne({username, "products.productName": productName, "products.colors": colors}, function (err, result) {
            if (err) throw err;
            resolve(result);
        });
    });
}

const updateQuantityGuest = (username, productName, colors, newQuantity) => {
    dbo.collection("guest").updateOne({username: username, "products.productName": productName, "products.colors": colors}, {$set: {"products.$.quantity": newQuantity}}, function(err, result) {
        if (err) throw err;
    });
}

const addToGuestCart = (username, photo, productName, price, colors, quantity) => {
    const product = {photo, productName, price, colors, quantity};
    dbo.collection("guest").updateOne({username}, {$push: {products: product}}, function(err, result) {
        if (err) throw err;
    });
}

const copyCartToUser = (username, products) => {
    dbo.collection("cart").insertOne({username, products}, function(err, result) {
        if (err) throw err;
    });
}

const deleteFromGuest = (username, productName, colors) => {
    return new Promise(resolve => {
        dbo.collection("guest").updateOne({username: username}, {$pull: {products: {productName: productName, colors: colors}}},
        function(err, result){
            if (err) throw err;
            resolve(result);
        });
    });
}

const deleteGuestCart = (username) => {
    dbo.collection("guest").deleteOne({username: username}, function(err, result) {
        if (err) throw err;
    });
}

module.exports = {guestCartExists, createGuestCart, checkGuestCart, updateQuantityGuest, addToGuestCart, copyCartToUser, deleteFromGuest, deleteGuestCart}
