const bcrypt = require("bcrypt");
const mongodb = require("mongodb").MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = "localhost:27017";
const DB = "dB";
const userPwd = "joulu:tonttu";
//?authSource=admin'
let dbo;

mongodb.connect(`mongodb://${userPwd}@${url}/${DB}?authSource=admin`, {useNewUrlParser: true, useUnifiedTopology: true}, (err, db) => {
    dbo = db.db("dB");
});

const saltRounds = 12;

const register = (email, username, password) => {
    bcrypt.hash(password, saltRounds, function (err, hash) {
            const obj = {email: email, username: username, password: hash};
            dbo.collection("users").insertOne(obj, function(err, result) {
                if (err) throw err;
        });
    });
}

const findUser = (username) => {
    return new Promise (resolve => {
            dbo.collection("users").findOne({username: username}, (err, result) => {
                if (err) throw err;
                resolve(result);
        });
    });
}

const existingMail = (email) => {
    return new Promise (resolve => {
        dbo.collection("users").findOne({email: email}, (err, result) => {
            if (err) throw err;
            resolve(result);
        })
    })
}
 
const confirmUser = (username, password) => {
    return new Promise (resolve => {
            dbo.collection("users").findOne({username: username}, (err, result) => {
                if (err) throw err;
                resolve(result);
        });
    });
}

const findMail = (id) => {
    return new Promise(resolve => { 
            dbo.collection("users").findOne({_id: new ObjectID(id)}, (err, result) => {
                if (err) throw err;
                resolve(result);
        });
    });
}

const findInfo = (username) => {
    return new Promise(resolve => {
            dbo.collection("profiles").findOne({username: username}, (err, result) => {
                if (err) throw err;
                resolve(result);
        });
    });
}

const checkProfile = (username) => {
    return new Promise (resolve => {
            dbo.collection("profiles").findOne({username: username}, (err, result) => {
                if (err) throw err;
                resolve(result);
        });
    });
}

const saveProfile = (username, email, fullName, shipAddress, billAddress) => {
            const obj = {username: username, email: email, fullName: fullName, shipAddress: shipAddress, billAddress: billAddress};
            dbo.collection("profiles").insertOne(obj, function(err, result){
                if(err) throw err;
                console.log("1 profile added");
        });
}

const updateProfile = (username, email, fullName, shipAddress, billAddress) => {
        dbo.collection("profiles").updateOne({username: username}, {$set: {fullName: fullName, shipAddress: shipAddress, billAddress: billAddress}}, function(err, result){
            if(err) throw err;
            console.log("1 profile updated");
    });
}

const feedback = (name, message) => {
    const feedback = {name: name, message: message};
    dbo.collection("feedback").insertOne(feedback, function(err, result) {
        if (err) throw err;
    });
}

const printFb = () => {
    return new Promise (resolve => {
            dbo.collection("feedback").find({}).toArray((err, result) => {
                if(err) throw err;
                resolve(result);
        });
    });
}

const getProducts = () => {
    return new Promise (resolve => {
            dbo.collection("tuotteet").find({}).toArray((err, result) => {
                if(err) throw err;
                resolve(result);
        });
    });
} 


module.exports = {register, findUser, existingMail, confirmUser, findMail, findInfo, checkProfile, updateProfile, saveProfile, feedback, printFb, getProducts}
