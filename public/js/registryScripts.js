const errors = window.errors;
const registryForm = window.registryForm;
const popup = document.getElementById("registryPopup");
const form = document.getElementById("registry");
const inputEmail = form.querySelector("input[name='email']");
const inputUsername = form.querySelector("input[name='username']");
const inputPassword = form.querySelector("input[name='password']");
const header = document.getElementsByTagName("header")[0];
const main = document.getElementsByTagName("main")[0];
const outsidePopup = main.querySelector(".log_in");

function loadAlert() {
    if (errors.length === 1) {
        popup.innerHTML += `<p class="popupHeadline">Rekisteröinnissä oli tämä ongelma:</p>`;
    } else {
        popup.innerHTML += `<p class="popupHeadline">Rekisteröinnissä oli nämä ongelmat:</p>`;
    }
    errors.map(item => {
        const object = item;
        for (let key in object) {
            popup.innerHTML += `<p>${object[key]}</p>`;
        }
    });
    popup.innerHTML += `<p class="ok-button" onClick="hideAlert()">OK</p>`;
    popup.style.visibility = "visible";
    header.style.pointerEvents = "none";
    outsidePopup.style.pointerEvents = "none";
}

function hideAlert() {
    popup.style.visibility = "hidden";
    header.style.pointerEvents = "auto";
    outsidePopup.style.pointerEvents = "auto";
}

window.onload = () => {
    let email = false;
    let username = false;
    let password = false;

    for (let i = 0; i < errors.length; i++) {
        if (errors[i].email) {
            email = true;
        } else if (errors[i].username) {
            username = true;
        } else if (errors[i].password) {
            password = true;
        }
    }

    if (email === false) {
        inputEmail.value = registryForm.email;
    }
    if (username === false) {
        inputUsername.value = registryForm.username;
    }
    if (password === false) {
        inputPassword.value = registryForm.password;
    }

    setTimeout(() => loadAlert(), 200);
};