Updated: 17.11.2020
The Taitava tonttu webshop is currently under redesinging.
The front page, the products page, the contact page, and the login page are updated.
  Their functionalities should work without problems. 
In the products page, the filters and the send-to-cart functionality work.   
  The send-to-whislist functionality (once logged in) needs revision.
The cart page shows the products added. 
  Empty cart and eliminate item functionalities work.
  Go to checkout is still pending revision.
The home page (once logged in) is showing its old, non-responsive version. 
  Functionalities may not work.
Other functionalities may still show bugs.