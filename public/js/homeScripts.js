const modify = document.getElementById("modify");
const box = document.getElementById("box5");
const box51 = document.getElementById("box51");
const box52 = document.getElementById("box52");
const box61 = document.getElementById("box61");

modify.addEventListener("click", () => {
    box.style.background = "linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('../images/wallpaper2.jpg')";

    box51.innerHTML = `
            <p>Full name: </p> <input type="text" id="fullName" name="fullName" value="${document.getElementById("fullName").innerHTML}">
            <p>Shipping address: </p> 
            <p>Address</p> <input type="text" id="shipAddress" name="shipAddress" value="${document.getElementById("shipAddress").innerHTML.split(",", 1).pop()}" >
            <p>City</p> <input type="text" id="shipCity" name="shipCity" value="${document.getElementById("shipAddress").innerHTML.split(",", 2).pop().trim().split(" ", 1)}" >
            <p>Postal code</p> <input type="text" id="shipPC" name="shipPC" value="${document.getElementById("shipAddress").innerHTML.split(",", 2).pop().split(" ").pop()}" >
            <p>Country</p> <input type="text" id="shipCountry" name="shipCountry" value="${document.getElementById("shipAddress").innerHTML.split(",").pop().trim()}" >
        `;
    box52.innerHTML = `            
            <p class="billing">Billing address: </p> 
            <p class="billing">Address</p> <input class="billing" type="text" id="billAddress" name="billAddress" value="${document.getElementById("billAddress").innerHTML.split(",", 1).pop()}" >
            <p class="billing">City</p> <input class="billing" type="text" id="billCity" name="billCity" value="${document.getElementById("billAddress").innerHTML.split(",", 2).pop().trim().split(" ", 1)}">
            <p class="billing">Postal code</p> <input class="billing" type="text" id=billPC" name="billPC" value="${document.getElementById("billAddress").innerHTML.split(",", 2).pop().split(" ").pop()}">
            <p class="billing">Country</p> <input class="billing" type="text" id="billCountry" name="billCountry" value="${document.getElementById("billAddress").innerHTML.split(",").pop().trim()}">
    `;
    box61.innerHTML = `            
            <p><input type="checkbox" name ="addressBox" id="addressBox" onClick="sameAddress()" />The shipping address and the billing address are the same.</p>
            <p><input type="submit" value="Save profile" /></p>
    `;
});
 
function sameAddress() {
    const addressBox = document.getElementById("addressBox");
    const billing = document.getElementsByClassName("billing");
    if (addressBox.checked != true) {
        let i;
        for (i=0; i < billing.length; i++) {
            billing[i].style.display = "block";
        }
    } else {
        let i;
        for (i=0; i < billing.length; i++) {
            billing[i].style.display = "none";
        }
    }
} 

const wishlist = document.getElementById("wishlist");
const orders = document.getElementById("orders");

wishlist.addEventListener("click", () => {
    box.style.background = "linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('../images/wallpaper2.jpg')";
    fetch("http://localhost:7000/home/getwishlist")
        .then(response => response.json())
        .then(wishlist => {
            if (wishlist.length === 0) {
                box.innerHTML= `
                <div>
                <p>Your wishlist is empty.</p>
                </div>
                `;
            } else {
                let wishlistProducts = wishlist[0].products;
                let myWishlist = [];
                for (let k = 0; k < wishlistProducts.length; k++) {
                    if (wishlistProducts[k].comment) {
                    myWishlist += '<form class="wlProRow" action="/home/toCart" method="POST"><div class="z"><input type="hidden" name="photo" value="' + wishlistProducts[k].photo + '"><img src="../../images/' + wishlistProducts[k].photo + '.jpg"></img></div> <div class="a"><p><input type="hidden" name="productName" value="' + wishlistProducts[k].productName + '">' + 
                    wishlistProducts[k].productName + '</p><p><input type="hidden" name="colors" value="' + wishlistProducts[k].colors + '">' + wishlistProducts[k].colors + '<p><input type="hidden" name="price" value="' + wishlistProducts[k].price + '">' + wishlistProducts[k].price +
                    '</p></div><div class="b"><p>Comment:</p><textarea rows="4" cols="50" name="comment">' + wishlistProducts[k].comment + '</textarea></div> <div class="c"><p><input type="hidden" name="quantity" value="' + wishlistProducts[k].quantity + '">' + 
                    wishlistProducts[k].quantity + '</p> <p><input type="submit" name="toCart" value="Add to cart"></p></div> <div class="d"><p><input type="submit" name="deleteB" formaction="/home/delete" value="Delete"></p> </div> </form>'
                    } else {
                        myWishlist += '<form class="wlProRow" action="/home/toCart" method="POST"><div class="z"><input type="hidden" name="photo" value="' + wishlistProducts[k].photo + '"><img src="../../images/' + wishlistProducts[k].photo + '.jpg"></img></div> <div class="a"><p><input type="hidden" name="productName" value="' + wishlistProducts[k].productName + '">' + 
                        wishlistProducts[k].productName + '</p><p><input type="hidden" name="colors" value="' + wishlistProducts[k].colors + '">' + wishlistProducts[k].colors + '<p><input type="hidden" name="price" value="' + wishlistProducts[k].price + '">' + wishlistProducts[k].price +
                        '</p></div><div class="b"><p>Comment:</p><textarea rows="4" cols="50" name="comment"></textarea></div> <div class="c"><p><input type="hidden" name="quantity" value="' + wishlistProducts[k].quantity + '">' + 
                        wishlistProducts[k].quantity + '</p> <p><input type="submit" value="Add to cart"></p></div> <div class="d"><p><input type="submit" formaction="/home/delete" value="Delete"></p> </div> </form>'
                    }
                }
                let comments = [];
                for (let n = 0; n > wishlistProducts.length; n++) {
                    const comment = document.getElementsByName("comment");
                    comments += '<form type="hidden" name="comments" value="' + comment[n].value + '">'
                }
        
                box.innerHTML=`
                ${myWishlist}
                <div class="wlProRow">
                    <form class="f" action="/home/updateWishlist" method="POST">
                        ${comments};
                        <input type="submit" value="Update my wishlist">
                    </form>
                    <form class="g"  action="/home/allToCart" method="POST">
                        <input type="submit" value="Add all to cart">
                    </form>
                    <form class="h" action="/home/deleteAll"  method="POST">
                        <input type="submit" value="Delete my wishlist">
                    </form>
                </div>
                `;
            }
        });
});


orders.addEventListener("click", () => {
    box.style.background = "linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('../images/wallpaper2.jpg')";
    fetch("http://localhost:7000/home/getOrders")
    .then(response => response.json())
    .then(orders => {
        console.log(orders);
        if(orders.length === 0) {
            box.innerHTML= `
            <p>Your history of orders is empty.</p>
            `;
        } else {
            let ordersHistory = [];
            for (let n = 0; n < orders.length; n++) {
                let productsList = [];
                for (let q = 0; q < orders[n].orders[1].productName.length; q++) {
                    productsList += '<dd>' + orders[n].orders[1].productName[q] 
                    + ' / ' + orders[n].orders[1].productColors[q]
                    + ' / ' + orders[n].orders[1].productQuantity[q] + 
                    ' item </dd> </dd>'
                }
                ordersHistory += '<div class="orders"><dl><dt>Date: ' + orders[n].date.split("T")[0] 
                + '</dt><dt>Products ordered: </dt>' + productsList + '</dt><dt>Final price: ' + orders[n].orders[2].finalPrice + '</dt></dl></div>'
            }
            box.innerHTML= `${ordersHistory}
            `;
        }
    });
});
