const {register, findUser, existingMail, confirmUser} = require("../scripts/scripts");
const express = require("express");
const route = express.Router();
const bcrypt = require("bcrypt");
const {body, validationResult} = require("express-validator");

//other variables needed
const pCL = 'a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ';
const regEx = new RegExp(`^(?=.*[0-9])(?=.*[${pCL}])([${pCL}0-9]+)$`);

route.get("/", (req, res) => {
    res.render("../../public/partials/login.ejs");
});

const registryRules = [
    //validation --email
    body("email", "Lisäää sähköpostisi").notEmpty(),
    body("email", "Sähköpostin formaatti ei ole oikeaa.").isEmail(),
    body("email", "Sähköpostiosoite on jo käytössä.").custom(async value => {
        const email = await existingMail(value);
        if (email !== null) {
            throw new Error("Sähköpostiosoite on jo käytössä.");
        }
    }),
    //validation -- username
    body("username", "Käyttäjänimissä täytty olla vähintään 5 merkkiä.").isLength({min: 5}),
    body("username", "Käyttäjänimi on jo käytössä.").custom(async value => {
        const username = await findUser(value);
        if (username !== null) {
            throw new Error("Käyttäjänimi on jo käytössä.");
        }
    }),
    //validation -- password
    body("password", "Salasanssa täytty olla vähintään 8 merkkiä.").isLength({min: 8}),
    body("password", "Salasanssa täytty olla numerioita ja kirjaimia.").matches(regEx)
];

route.post("/", registryRules, (req, res) => {
    const {email, username, password} = req.body;
    const registryForm = {email, username, password};

    //errors
    const errors = validationResult(req);
    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));
    if (extractedErrors.length !== 0) {
        res.render("../../public/partials/registry.ejs", {
            errors: extractedErrors,
            registryForm
        });
    } else {
        register(email, username, password);
        res.render("../../public/partials/log.ejs", {
            username
        });
    }
});

let userPwd;
const loginRules = [
    //validation
    body("username", "Lisää käyttäjänimisi tähän.").notEmpty(),
    body("username").custom(async value => {
        userPwd = await confirmUser(value);
        if (userPwd === null) {
            throw new Error("Käyttäjänimi ei ole olemassa.");
        }
    }),
    body("password", "Lisää salasana tähän.").notEmpty(),
    body("password").custom(value => {
        return bcrypt.compare(value, userPwd.password)
        .then(result => {
            if (!result) {
                return Promise.reject("wrong password");
            }
        });
    })
];

route.post("/log", loginRules, (req, res) => {
    const {username} = req.body;
    
    //errors
    const errors = validationResult(req);
    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));
    if (extractedErrors.length !== 0) {
        res.render("../../public/partials/log.ejs", {
            errors: extractedErrors,
            username
        });
    } else {
        req.session.userId = userPwd._id;
        req.session.username = userPwd.username;
        res.redirect("/home");
    }
});

module.exports = route;