const errors = window.errors;
const username = window.username;
const popup = document.getElementById("logPopup");
const form = document.getElementById("log");
const inputUsername = form.querySelector("input[name='username']");
const header = document.getElementsByTagName("header")[0];
const main = document.getElementsByTagName("main")[0];
const outsidePopup = main.querySelector(".log_in");

function loadLoginAlert() {
    popup.innerHTML = `<p class="popupHeadline">Rekisteröi meni hyvin!</p>
    <p>Nyt voit kirjautua sisään Taivatan tontun mökiin.</p>
    <p class="ok-button" onClick="hideAlert()">OK</p>`;
    popup.style.visibility = "visible";
    header.style.pointerEvents = "none";
    outsidePopup.style.pointerEvents = "none";
}

function loadPasswordAlert() {
    popup.innerHTML = `<p class="popupHeadline">Kirjaudu sisään ongelma.</p>`;
    if (errors.length === 1) {
        errors.map(item => {
            const object = item;
            for (let key in object) {
                popup.innerHTML += `<p>${object[key]}</p>`;
            }
        });
    } else {
        popup.innerHTML += `<p>${errors[0].username}</p>`;
    }
    popup.innerHTML += `<p class="ok-button" onClick="hideAlert()">OK</p>`;
    popup.style.visibility = "visible";
    header.style.pointerEvents = "none";
    outsidePopup.style.pointerEvents = "none";
}

function hideAlert() {
    popup.style.visibility = "hidden";
    header.style.pointerEvents = "auto";
    outsidePopup.style.pointerEvents = "auto";
}

window.onload = () => {
    if (errors) {
        setTimeout(() => loadPasswordAlert(), 200);
    } else {
        setTimeout(() => loadLoginAlert(), 200);
    }
    inputUsername.value = username;
    
};