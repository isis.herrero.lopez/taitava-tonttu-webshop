const welcomeSection = document.getElementsByClassName("welcome")[0];
const infoSection = document.getElementsByClassName("info")[0];
const main = document.getElementsByTagName("main")[0];

if (window.matchMedia("(max-width: 600px)").matches){
    welcomeSection.addEventListener("click", displayChange);
    infoSection.addEventListener("click", displayChange);
}

function displayChange(){
    if (this.className === "welcome"){
        if (welcomeSection.children[1].style.display !== "none"){
            welcomeSection.children[1].style.display = "none";
            welcomeSection.children[2].style.display = "block";
            welcomeSection.children[3].style.display = "block";
        } else {
            welcomeSection.children[1].style.display = "block";
            welcomeSection.children[2].style.display = "none";
            welcomeSection.children[3].style.display = "none";
        }
    } else if (this.className === "info"){
        if (infoSection.children[1].style.display !== "none"){
            infoSection.children[1].style.display = "none";
            infoSection.children[2].style.display = "block";
            infoSection.children[3].style.display = "block";
        } else {
            infoSection.children[1].style.display = "block";
            infoSection.children[2].style.display = "none";
            infoSection.children[3].style.display = "none";
        }
    }
}

/*json file*/
const noveltiesBox = document.getElementsByClassName("noveltiesBox")[0];
fetch("./json/novelties.json")
    .then(res => {
        return res.json()
    })
    .then(data => {
        for (let i = 0; i < data.news.length; i++){
            const idNumber = - i;
            noveltiesBox.innerHTML += `<div class="novelty" id="${idNumber}">
                                        <div class="noveltyText">
                                            <h4>${data.news[i].title}</h4>
                                            <p>${data.news[i].text}</p>
                                            <h5>${data.news[i].date}</h5>
                                        </div>
                                       </div>`;
            if (data.news[i].image){
                const parent = document.getElementById(idNumber);
                parent.innerHTML += `<div class="noveltyImage" id="image${idNumber}">
                                        <img src="${data.news[i].image}" alt="news ${data.news[i].date}" id="img${idNumber}">
                                    </div>`;
                parent.children[0].className = "noveltyText1";
                parent.children[0].id = "text" + idNumber;
                const text = document.getElementById("text" + idNumber);
                const height = text.offsetHeight;
                const width = text.offsetWidth;
            
                const noveltyImage = document.getElementById("image" + idNumber);
                const image = document.getElementById("image" + idNumber).children[0];
                noveltyImage.style.width = width + "px";
                noveltyImage.style.height = height + "px";
                image.style.height = height + "px";
                const top = parent.offsetHeight - noveltyImage.offsetHeight;
                noveltyImage.style.top = (top/4) + "px";
            }
        }
    })
    .then(() => {
        const noveltyGroup = document.getElementsByClassName("novelty");
        for (let i = 0; i < noveltyGroup.length; i++){
            const id = noveltyGroup[i].id;
            document.getElementById(id).style.order = noveltyGroup[i].id;
        } 
    }).then(() => {
        const noveltyGroup = document.getElementsByClassName("novelty");
        for (let i = 0; i < noveltyGroup.length; i++){
            if (noveltyGroup[i].children.length === 2){
                noveltyGroup[i].addEventListener("mouseleave", twirlBack);
            }
        }
    //not working very well....
        function twirlBack(e){
            /*const text = e.target.children[0];
            const image = e.target.children[1];
            console.log(text, image);
            text.style.animation = "twirl 0.3s ease-in forwards reverse";
            image.style.animation = "twirl 0.3s ease-in forwards";

            setTimeout(() => (text.removeAttribute(animation)), 1000);
            setTimeout(() => (image.removeAttribute(animation)), 1000);*/
        }
    });

window.onload = function(){
    if (window.matchMedia("(min-width: 1025px)").matches){
        const welcomeImage = document.createElement("div");
        welcomeImage.className = "indexImg";
        main.insertBefore(welcomeImage, infoSection);
    }
}

window.onresize = function(){
    if (window.matchMedia("(min-width: 1025px)").matches){
        const main = document.body.children[1];
        if (main.children.length === 3){
            const welcomeImage = document.createElement("div");
            welcomeImage.className = "indexImg";
            main.insertBefore(welcomeImage, infoSection);
        }
    }

    /*write her a funciton for rezising the image of novelties*/
    const noveltyGroup = document.getElementsByClassName("novelty");
    for (let i = 0; i < noveltyGroup.length; i++){
        if (noveltyGroup[i].children.length === 2){
            const noveltyText = noveltyGroup[i].children[0];
            const noveltyImage = noveltyGroup[i].children[1];
            if (noveltyText.offsetHeight !== noveltyImage.offsetHeight){
                console.log("diff height; resize image");
                noveltyImage.style.height = noveltyText.offsetHeight + "px";
                noveltyImage.children[0].style.height = noveltyText.offsetHeight + "px";
            }
        }
    }
    
}

