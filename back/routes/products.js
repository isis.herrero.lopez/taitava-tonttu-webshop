const { getProducts, findMail } = require("../scripts/scripts");
const { saveWishlist, checkWishlist, updateWishlist, checkWishlistProducts, quantityWishlist, addToCart, checkCart, addMoreToCart, checkCartProducts, quantityCart } = require("../scripts/moreScripts");
const {guestCartExists, createGuestCart, checkGuestCart, updateQuantityGuest,  addToGuestCart} = require ("../scripts/guestScripts");
const express = require("express");
const route = express.Router();
const {body, validationResult} = require("express-validator");

/*
route.get("/", (req, res) => {
    res.sendFile("products.html", {
        root: __dirname + "/../../public/html"
    });
});
*/

route.get("/", async (req, res) => {
    const products = await getProducts();
    res.render("../../public/partials/products.ejs", { products: products });
});

//validators
const cartRules = [
    body("colors").custom(value => {
        if (value === undefined) {
            throw Error("Please, select colors");
        } else {
            return value;
        }
    }),
    body("quantity").custom(value => {
        if (value === undefined) {
            throw Error("Please, select a quantity");
        } else {
            return value;
        }
    })
]

route.post("/toWishlist", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const { photo, productName, price, colors, quantity } = req.body;
    const result = await checkWishlist(username);
    if (result === null) {
        console.log("no wishlist yet --> save it");
        saveWishlist(username, photo, productName, price, colors, quantity);
    } else {
        console.log("update wishlist");
        console.log(req.body.productName);
        const newResult = await checkWishlistProducts(username, productName, colors);
        if (newResult === null) {
            updateWishlist(username, photo, productName, price, colors, quantity);
        } else {
            let newQuantity = +req.body.quantity + +result.products[0].quantity;
            quantityWishlist(username, productName, colors, newQuantity);
        }
    }
    res.redirect("/products");
});

route.post("/toCart", cartRules, async (req, res) => {
    const user = await findMail(req.session.userId);
    const { photo, productName, price, colors, quantity } = req.body;

    //errors
    const errors = validationResult(req);
    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));
    if (extractedErrors.length !== 0) {
        const products = await getProducts();
        res.render("../../public/partials/products.ejs", {
            products,
            errors: extractedErrors
        });
    } else {
        if (user){
            const username = user.username;
            const result = await checkCart(username);
            if (result === null) {
                addToCart(username, photo, productName, price, colors, +quantity);
            } else {
                const newResult = await checkCartProducts(username, productName, colors);
                if (newResult === null) {
                    addMoreToCart(username, photo, productName, price, colors, +quantity);
                } else {
                    let newQuantity = +req.body.quantity + +result.products[0].quantity;
                    quantityCart(username, productName, colors, newQuantity);
                }
            }
        } else {
            const username = req.signedCookies.thisCookie;
            const result = await guestCartExists(username);
            if (result === null) {
                createGuestCart(username, photo, productName, price, colors, quantity);
            } else {
                const newResult = await checkGuestCart(username, productName, colors);
                if (newResult === null) {
                    addToGuestCart(username, photo, productName, price, colors, quantity);
                } else {
                    let newQuantity = +quantity + + newResult.products[0].quantity;
                    updateQuantityGuest(username, productName, colors, newQuantity);
                }
            }
        }
        res.redirect("/products");
    }
});

module.exports = route;