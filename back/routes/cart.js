const { findMail } = require("../scripts/scripts");
const { checkCart, deleteFromCart, emptyCart } = require("../scripts/moreScripts");
const { guestCartExists, deleteFromGuest, deleteGuestCart } = require("../scripts/guestScripts");
const express = require("express");
const route = express.Router();

route.get("/", async (req, res) => {
    const user = await findMail(req.session.userId);
    if (user === null) {
        const username = req.signedCookies.thisCookie;
        const result = await guestCartExists(username);
        if (result === null || result.products.length === 0) {
            res.render("../../public/partials/cart.ejs", {result: null});
        } else {
            res.render("../../public/partials/cart.ejs", {result: result});
        }
    } else {
        const username = user.username;
        const result = await checkCart(username);
        if (result === null || result.products.length === 0) {
            res.render("../../public/partials/cart.ejs", {result: null});
        } else {
            res.render("../../public/partials/cart.ejs", {result: result});
        }
    }
}); 

route.post("/delete", async (req, res) => {
    const user = await findMail(req.session.userId);
    if (user === null) {
        const username = req.signedCookies.thisCookie;
        const productName = req.body.productName;
        const colors = req.body.colors;
        await deleteFromGuest(username, productName, colors);
    } else {
        const username = user.username;
        const productName = req.body.productName;
        const colors = req.body.colors;
        await deleteFromCart(username, productName, colors);
    }
    res.redirect("/cart");
});

route.post("/emptyCart", async (req, res) => {
    const user = await findMail(req.session.userId);
    if (user === null) {
        const username = req.signedCookies.thisCookie;
        await deleteGuestCart(username);
    } else {
        const username = user.username;
        await emptyCart(username);
    }
    res.redirect("/cart");
});

route.post("/checkout", (req, res) => {
    const total = req.body.finalPrice;
    req.session.total = total;
    res.redirect("/checkout");
});

module.exports = route; 