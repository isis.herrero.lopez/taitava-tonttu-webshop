const {checkProfile, saveProfile, updateProfile, findMail, findInfo} = require("../scripts/scripts");
const {printWishlist, printOrders, addComments, deleteFromWishlist, emptyWishlist, checkWishlist, checkCartProducts, quantityCart, addToCart, checkCart, addMoreToCart} = require("../scripts/moreScripts");
const express = require("express");
const route = express.Router();

route.get("/", async (req, res) => {
    const user = await findMail(req.session.userId);
    const userInfo = await findInfo(req.session.username);
    if (userInfo === null) {
        res.render("../../public/partials/home.ejs",{
            email: user.email,
            fullName: "",
            shipAddress: "",
            billAddress: ""
        });
    } else {
        res.render("../../public/partials/home.ejs",{
            email: user.email,
            fullName: userInfo.fullName,
            shipAddress: userInfo.shipAddress,
            billAddress: userInfo.billAddress
        });
    }
});
 
route.post("/saveProfile", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const email = user.email;
    const fullName = req.body.fullName;
    const shipAddress = req.body.shipAddress + ", " + req.body.shipCity + " " + req.body.shipPC + ", " + req.body.shipCountry;
    let billAddress;
    if (req.body.addressBox) {
        console.log("addressBox checked");
        billAddress = shipAddress;
    } else {
        console.log("checkBox not checked");
        billAddress = req.body.billAddress + ", " + req.body.billCity + " " + req.body.billPC + ", " + req.body.billCountry; 
    }
    const oldProfile = await checkProfile(username);
        if (oldProfile === null) {
            console.log("non-existing profile --> save");
            saveProfile(username, email, fullName, shipAddress, billAddress);
        } else {
            console.log("old profile --> update");
            updateProfile(username, email, fullName, shipAddress, billAddress);
        }
    res.redirect("/home");
})


route.get("/getwishlist", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const wishlist = await printWishlist(username);
    res.send(wishlist);
});

route.get("/getOrders", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const orders = await printOrders(username);
    res.send(orders);
});

route.post("/toCart", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const {photo, productName, price, colors, quantity} = req.body;
    const cart = await checkCart(username);
    if (cart){
        const result = await checkCartProducts(username, productName, colors);
        if (result === null) {
            addMoreToCart(username, photo, productName, price, colors, quantity);
        } else {
            let newQuantity = +req.body.quantity + +result.products[0].quantity;
            quantityCart(username, productName, colors, newQuantity)
        }
    } else {
        addToCart(username, photo, productName, price, colors, quantity);
    }
    await deleteFromWishlist(username, productName, colors);
    res.redirect("/home");
});

route.post("/delete", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    let {productName, colors} = req.body;
    await deleteFromWishlist(username, productName, colors);
    const result = await checkWishlist(username);
    if (result.products.length === 0) {
        console.log("empty wishlist");
        await emptyWishlist(username);
    } else {
        console.log("wishlist with products");
    }
    res.redirect("/home");
});

route.post("/updateWishlist", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const wishlist = await printWishlist(username);
    const products = wishlist[0].products;
    let productName, colors, comment;
    for (let i = 0; i < products.length; i++) {
        productName = products[i].productName;
        colors = products[i].colors;
        comment= req.body.comments;
        console.log(comment);
        //addComments(username, productName, colors, comment);
    }
    res.redirect("/home");
});

route.post("/allToCart", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    const wishlist = await printWishlist(username);
    const products = wishlist[0].products;
    let photo, productName, colors, quantity, price;
    for (let i =0; i < products.length; i++) {
        photo = products[i].photo;
        productName = products[i].productName;
        price = products[i].price;
        colors = products[i].colors;
        quantity = products[i].quantity;
        console.log(photo);
        const cart = await checkCart(username);
        if (cart){
            const result = await checkCartProducts(username, productName, colors);
            if (result === null) {
                addMoreToCart(username, photo, productName, price, colors, quantity);
            } else {
                let newQuantity = +req.body.quantity + +result.products[0].quantity;
                quantityCart(username, productName, colors, newQuantity)
            }
        } else {
            addToCart(username, photo, productName, price, colors, quantity);
        }
    }
    await emptyWishlist(username);
    res.redirect("/home");
});

route.post("/deleteAll", async (req, res) => {
    const user = await findMail(req.session.userId);
    const username = user.username;
    await emptyWishlist(username);
    res.redirect("/home");
});

module.exports = route;