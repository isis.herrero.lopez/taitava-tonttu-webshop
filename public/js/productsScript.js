/*last revision 15.10.2020*/
const productBoxes = document.getElementsByClassName("productBox");

function anyVisibleBox(boxesArray) {
    const visibleBoxes = boxesArray.filter(item => item.style.display === "block");
    const emptyMessage = document.getElementById("emptyMessage");
    if (visibleBoxes.length === 0) {
        emptyMessage.style.display = "block";
    } else {
        emptyMessage.style.setProperty("display", "none");
    }
}

function showOrHide() {
    const boxesArray = [...productBoxes];
    if (activeFilters.length === 0) {
        boxesArray.map(item => document.getElementById(item.id).style.display = "block");
    } else {
        boxesArray.forEach(item => {
            const keywords = item.getAttribute("keywords").split(",");
            let hide = false;
            for (let i = 0; i < activeFilters.length; i++) {
                for (let k = 0; k < keywords.length; k++) {
                    if (activeFilters[i] === keywords[k]) {
                        break;
                    } else {
                        if (k === keywords.length - 1) {
                            hide = true;
                        }
                    }
                }
                if (hide === true) {
                    break;
                }
            }
            if (hide === true) {
                document.getElementById(item.id).style.display = "none";
            } else {
                document.getElementById(item.id).style.display = "block";
            }
        });
        anyVisibleBox(boxesArray);
    }
}

let activeFilters = [];
function filterChange(filter) {
    if (filter.checked) {
        activeFilters.push(filter.name.slice(0, -4));
        showOrHide();
    } else {
        activeFilters = activeFilters.filter(item => item !== filter.name.slice(0, -4));
        showOrHide();
    }
}

const popup = document.getElementById("productsPopup");
const header = document.getElementsByTagName("header")[0];
const main = document.getElementsByTagName("main")[0];

const errors = window.errors;
function loadAlert() {
    popup.innerHTML = `<p class="popupHeadline">Ongelma</p>`;
    errors.map(item => {
        const object = item;
        for (let key in object) {
            popup.innerHTML += `<p>${object[key]}</p>`;
        }
    });
    popup.innerHTML += `<p class="ok-button" onClick="hideAlert()">OK</p>`;
    popup.style.visibility = "visible";
    popup.style.pointerEvents = "auto";
    header.style.pointerEvents = "none";
    main.style.pointerEvents = "none";
}

function hideAlert() {
    popup.style.visibility = "hidden";
    header.style.pointerEvents = "auto";
    main.style.pointerEvents = "auto";
}

window.onload = () => {
    if (errors) {
        setTimeout(() => loadAlert(), 200);
    }
}
